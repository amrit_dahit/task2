/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anuz.fest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author anuz
 */
@ManagedBean(name = "student")
@SessionScoped
public class Student implements Serializable {
    
    private String fname;
    private String mname;
    private String lname;
    private String faculty;
    private String program;
    
    private Map<String, Map<String,String>> data = new HashMap<String, Map<String, String>>();
        
    private Map<String,String> faculties;
    private Map<String,String> programs;
    
    private ArrayList<Student> studentList;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public Map<String, String> getFaculties() {
        return faculties;
    }

    public void setFaculties(Map<String, String> faculties) {
        this.faculties = faculties;
    }

    public Map<String, String> getPrograms() {
        return programs;
    }

    public void setPrograms(Map<String, String> programs) {
        this.programs = programs;
    }

    public ArrayList<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(ArrayList<Student> studentList) {
        this.studentList = studentList;
    }

    public Student() {   
        
    }
   
    
    @PostConstruct
    public void init(){
        studentList= new ArrayList<>();
        
        
        faculties = new HashMap<>();
        faculties.put("Management","Management");
        faculties.put("Science and Techology", "Science and Technology");
        
        Map<String,String> map = new HashMap<>();
        map.put("BBA", "BBA");
        map.put("BBS", "BBS");
        data.put("Management", map);
        
        
        map = new HashMap<>();
        map.put("BE Computer", "BE Computer");
        map.put("BCA", "BCA");
        data.put("Science and Technology", map);
    }
    
     public void onFacultyChange(){
         
         // it means this method is called
         
         if(faculty != null && !faculty.equals("")){
             programs = data.get(faculty);
         }else{
             programs = new HashMap<>();     
         }
            
     }
     
     
     
     
     
     public void saveInfo(){
         
         Student s =new Student();
         
         s.setFname(fname);
         s.setMname(mname);
         s.setLname(lname);
         s.setFaculty(faculty);
         s.setProgram(program);
         
         studentList.add(s);
         
         fname=null;
         mname=null;
         lname=null;
         faculty=null;
         onFacultyChange();
         program=null;
     
     
    
     }
    
    public void delete(Student s){
    
    studentList.remove(s);
    
     
    }
    
    
    public void edit(Student s){
        
       
      
           this.fname=s.fname;
        this.mname=s.mname;
       this.lname=s.lname;
        this.faculty=s.faculty;
        onFacultyChange();
    this.program=s.program;
    
        
        
       delete(s);
      
    
    }
    
}
